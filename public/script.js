(function() {
    var firebaseConfig = {
        apiKey: "AIzaSyAXzbdvJQQjstr3Nuw_m8SEN_H_-EL8UX4",
        authDomain: "garretslawncare.firebaseapp.com",
        databaseURL: "https://garretslawncare.firebaseio.com",
        projectId: "garretslawncare",
        storageBucket: "garretslawncare.appspot.com",
        messagingSenderId: "998603236370",
        appId: "1:998603236370:web:59414e3d1d14fa9b32e4cb",
        measurementId: "G-4CYCWGZ60Y"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    const database = firebase.database();

    const preObject = document.getElementById('object');

    const dbRefObject = firebase.database().ref().child('object');

    dbRefObject.on('value', snap => preObject.innerText = JSON.stringify(snap.val(), null, 3));

}());